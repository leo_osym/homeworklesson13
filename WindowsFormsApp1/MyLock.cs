﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    delegate void UnlockDelegate();
    /// <summary>
    /// Class which checks code
    /// </summary>
    internal class MyLock
    {
        char[] codeArr;
        string Code;

        public MyLock(string code)
        {
            codeArr = code.ToCharArray();
            Code = code;
        }
        public event UnlockDelegate Unlock;
        public int Check(string code)
        {
            if (code.Equals(Code))
            {
                Unlock?.Invoke();
                return 1;
            }

            else if (code.Length.Equals(Code.Length))
                return -1;
            return 0;
        }
    }
}
