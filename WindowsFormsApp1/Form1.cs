﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : System.Windows.Forms.Form
    {
        bool enableClosing = false;
        private static string realCode = "*#123654#";
        MyLock @lock = new MyLock(realCode);
        private string Code = "";

        private int Count=0;
        private int NumberOfAttempts = 5;

        public Form1()
        {
            @lock.Unlock += lock_Unlock;
            InitializeComponent();
        }
        private void lock_Unlock()
        {
            enableClosing = true;
            Close();
        }

        private void button_Click(object sender, EventArgs e)
        {
            Button b = sender as Button;
            Code += b.Text;
            textBox1.Clear();
            textBox1.AppendText(Code);
            showInfo(@lock.Check(Code));
        }

        private void showInfo(int i)
        {
            label2.Text = "";
            if (i==1)
            {
                label2.ForeColor = Color.Green;
                label2.Text = "You got it!";
                Code = "";
                textBox1.Clear();
            }
            else if (i == -1)
            {
                Count++;
                label2.ForeColor = Color.Red;
                label2.Text = String.Format($"Wrong password! You have {NumberOfAttempts - Count} attempts left");
                Code = "";
                textBox1.Clear();
            }
            if((NumberOfAttempts - Count) == 0)
            {
                textBox1.Clear();
                Code = "";
                textBox1.AppendText("Whoaha! Looser!"); // and do realy bad staff
                Thread.Sleep(10000);
                textBox1.Clear();
                Count = 0;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (enableClosing) return;
            e.Cancel = true;
        }
    }
}
